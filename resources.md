---
layout: home
title: Resources
permalink: /resources/
---


{% for item in site.data.nav.toc %}
  <h1>{{ item.title }}</h1>
  <ul>
  {% for entry in item.pages %}
  <li><a href="#">{{ entry.title | markdownify }}</a></li>
  {% endfor %}
  </ul>
{% endfor %}


{% if site.data.nav.toc2[0] %}

{% for item in site.data.nav.toc2%}
  <h3>{{ item.title }}</h3>
  <ul>
{% for entry in item.pages %}
  <li><a href="#">{{ entry.title}}</a></li>
  {% endfor %}
  </ul>

{% endfor %}
{% endif %}